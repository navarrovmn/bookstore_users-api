package services

import (
	"github.com/navarrovmn/bookstore_users-api/domain/users"
	"github.com/navarrovmn/bookstore_users-api/utils/errors"
)

// CreateUser service
func CreateUser(user users.User) (*users.User, *errors.RestError) {
	if err := user.Validate(); err != nil {
		return nil, err
	}
	if err := user.Save(); err != nil {
		return nil, err
	}

	return &user, nil
}

// GetUser service
func GetUser(userID int64) (*users.User, *errors.RestError) {
	result := &users.User{ID: userID}
	if err := result.Get(); err != nil {
		return nil, err
	}

	return result, nil
}

// UpdateUser service
func UpdateUser(isPartial bool, user users.User) (*users.User, *errors.RestError) {
	current, err := GetUser(user.ID)
	if err := user.Get(); err != nil {
		return nil, err
	}

	if isPartial {
		if user.FirstName != "" {
			current.FirstName = user.FirstName
		}
		if user.LastName != "" {
			current.LastName = user.FirstName
		}
		if user.Email != "" {
			current.Email = user.FirstName
		}
	} else {
		current.FirstName = user.FirstName
		current.LastName = user.LastName
		current.Email = user.Email
	}

	if err = current.Update(); err != nil {
		return nil, err
	}

	return current, nil
}

// DeleteUser from database
func DeleteUser(userID int64) *errors.RestError {
	user := &users.User{ID: userID}
	return user.Delete()
}
