package users

import (
	"fmt"

	dateutils "github.com/navarrovmn/bookstore_users-api/utils/date"

	// Test connection to database
	userdb "github.com/navarrovmn/bookstore_users-api/datasources/mysql/user_db"

	"github.com/navarrovmn/bookstore_users-api/utils/errors"
)

const (
	queryInsertUser = "INSERT INTO users(first_name, last_name, email, date_created) VALUES(?, ?, ?, ?;"
	queryGetUser    = "SELECT id, first_name, last_name, email, date_created FROM users WHERE id=?;"
	queryUpdateUser = "UPDATE users SET first_name=?, last_name=?, email=? WHERE id=?;"
	queryDeleteUser = "DELETE FROM users WHERE id=?;"
)

var (
	usersDB = make(map[int64]*User)
)

// Get a user from database
func (user *User) Get() *errors.RestError {
	statement, err := userdb.Client.Prepare(queryGetUser)
	if err != nil {
		return errors.NewInternalServerError(err.Error())
	}
	defer statement.Close()

	result := statement.QueryRow(user.ID)
	if err := result.Scan(&user.ID, &user.FirstName, &user.LastName, &user.Email, &user.DateCreated); err != nil {
		fmt.Println(err)
		return errors.NewInternalServerError(fmt.Sprintf("error when trying to get user %d: %s", user.ID, err.Error()))
	}

	return nil
}

// Save a user in database
func (user *User) Save() *errors.RestError {
	statement, err := userdb.Client.Prepare(queryInsertUser)
	if err != nil {
		return errors.NewInternalServerError(err.Error())
	}
	defer statement.Close()

	user.DateCreated = dateutils.GetNowString()
	insertResult, err := statement.Exec(user.FirstName, user.LastName, user.Email, user.DateCreated)
	if err != nil {
		return errors.NewInternalServerError(err.Error())
	}

	userID, _ := insertResult.LastInsertId()
	user.ID = userID

	return nil
}

// Update a user
func (user *User) Update() *errors.RestError {
	stmt, err := userdb.Client.Prepare(queryUpdateUser)
	if err != nil {
		return errors.NewInternalServerError(err.Error())
	}
	defer stmt.Close()

	_, err = stmt.Exec(user.FirstName, user.LastName, user.Email, user.ID)
	if err != nil {
		return errors.NewInternalServerError("cannot update user")
	}

	return nil
}

// Delete a user
func (user *User) Delete() *errors.RestError {
	stmt, err := userdb.Client.Prepare(queryDeleteUser)
	if err != nil {
		return errors.NewInternalServerError(err.Error())
	}
	defer stmt.Close()

	_, err = stmt.Exec(user.ID)
	if err != nil {
		return errors.NewInternalServerError("cannot update user")
	}
	return nil
}
