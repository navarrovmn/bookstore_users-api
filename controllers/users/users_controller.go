package users

import (
	"net/http"
	"strconv"

	"github.com/navarrovmn/bookstore_users-api/services"
	"github.com/navarrovmn/bookstore_users-api/utils/errors"

	"github.com/gin-gonic/gin"
	"github.com/navarrovmn/bookstore_users-api/domain/users"
)

// CreateUser function to create user in API
func CreateUser(c *gin.Context) {
	var user users.User
	if err := c.ShouldBindJSON(&user); err != nil {
		restErr := errors.NewBadRequestError("Invalid JSON body")
		c.JSON(restErr.Code, restErr)
		return
	}

	result, saveError := services.CreateUser(user)
	if saveError != nil {
		c.JSON(saveError.Code, saveError)
		return
	}

	c.JSON(http.StatusCreated, result)
}

// GetUser from API
func GetUser(c *gin.Context) {
	userID, userErr := strconv.ParseInt(c.Param("user_id"), 10, 64)
	if userErr != nil {
		err := errors.NewBadRequestError("user_id must be a number")
		c.JSON(err.Code, err)
		return
	}

	user, getErr := services.GetUser(userID)
	if getErr != nil {
		c.JSON(getErr.Code, getErr)
	}

	c.JSON(http.StatusOK, user)
}

// UpdateUser from API
func UpdateUser(c *gin.Context) {
	userID, userErr := strconv.ParseInt(c.Param("user_id"), 10, 64)
	if userErr != nil {
		err := errors.NewBadRequestError("user_id must be a number")
		c.JSON(err.Code, err)
		return
	}

	var user users.User
	if err := c.ShouldBindJSON(&user); err != nil {
		restErr := errors.NewBadRequestError("Invalid JSON body")
		c.JSON(restErr.Code, restErr)
		return
	}
	user.ID = userID

	isPartial := c.Request.Method == http.MethodPatch

	result, err := services.UpdateUser(isPartial, user)
	if err != nil {
		c.JSON(err.Code, err)
		return
	}

	c.JSON(http.StatusOK, result)
}

// Delete from API
func Delete(c *gin.Context) {
	userID, userErr := strconv.ParseInt(c.Param("user_id"), 10, 64)
	if userErr != nil {
		err := errors.NewBadRequestError("user_id must be a number")
		c.JSON(err.Code, err)
		return
	}

	if err := services.DeleteUser(userID); err != nil {
		c.JSON(err.Code, err)
		return
	}

	c.JSON(http.StatusOK, map[string]string{"status": "deleted"})
}
