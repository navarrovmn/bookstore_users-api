package userdb

import (
	"database/sql"
	"fmt"
	"log"

	// Import and not use it
	_ "github.com/go-sql-driver/mysql"
)

// This is the correct way to set the connection to the database. As we are just playing around, ignore it.
// const (
// 	my_sql_username = "mysql_users_username"
// 	my_sql_password = "mysql_users_password"
// 	mysql_users_host = "mysql_users_host"
// 	mysql_users_schema = "mysql_users_schema"
// )

// UserDB instance
var (
	Client *sql.DB

	// username := os.Getenv(my_sql_username)
	// password := os.Getenv(my_sql_password)
	// host := os.Getenv(mysql_users_host)
	// schema = := os.Getenv(mysql_users_schema)
)

func init() {
	datasourceName := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8",
		"victornavarro",
		"password",
		"127.0.0.1",
		"users_db")
	UsersDB, err := sql.Open("mysql", datasourceName)

	if err != nil {
		panic(err)
	}
	if err = UsersDB.Ping(); err != nil {
		panic(err)
	}

	log.Println("database successfully configured")
}

// LogInit friendly
func LogInit() {
	log.Println("starting database...")
}
