package errors

import "net/http"

// RestError represents an invalid result from an endpoint
type RestError struct {
	Message string
	Code    int
	Error   string
}

// NewBadRequestError boilerplate
func NewBadRequestError(message string) *RestError {
	return &RestError{
		Message: message,
		Code:    http.StatusBadRequest,
		Error:   "bad_request",
	}
}

// NewNotFoundError boilerplate
func NewNotFoundError(message string) *RestError {
	return &RestError{
		Message: message,
		Code:    http.StatusNotFound,
		Error:   "not_found",
	}
}

// NewInternalServerError boilerplate
func NewInternalServerError(message string) *RestError {
	return &RestError{
		Message: message,
		Code:    http.StatusInternalServerError,
		Error:   "internal_server_error",
	}
}
